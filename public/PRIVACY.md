## Privacy Policy
- The X Wallet webpage is static HTML/CSS page. There are no cookies, and there is no javascript. We do not intentionally or knowingly record or store any data about your visit.
- You should assume third parties are monitoring and recording all signals, including the requests you send to the server which hosts this website.  

We reserve the right to update this policy at any time, and make no guarantees as to the accuracy or availability of the content.
